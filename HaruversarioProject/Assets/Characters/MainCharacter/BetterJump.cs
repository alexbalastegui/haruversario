using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetterJump : MonoBehaviour
{
    [Tooltip("This value determines the amount of speed needed to start applying 'fallMultiplier'")]
    public float velocityThreshold = 0;
    [Tooltip("When vertical velocity falls bellow 0, this value is added to the gravity, to increase acceleration to the floor.")]
    public float fallMultiplier=2.5f;
    [Tooltip("When the rigidbody still holds vertical upwards velocity, and the SPACE input is released, this value is added to the gravity to allow the player to perform 'small jumps'")]
    public float lowJumpMultiplier=2f;

    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rb.velocity.y < velocityThreshold)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > velocityThreshold && !Input.GetButton("Jump"))
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
    }
}
