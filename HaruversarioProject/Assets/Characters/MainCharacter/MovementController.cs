using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    [Tooltip("The walk speed is interpolated with lerp. WalkAcc works as alpha and walkSpeed as desired speed.")]
    [Header("Walk")]

    public float walkSpeed=1;
    public float walkAcc = 0.5f;
    public float brakeDeacc = 0.5f;

    private float myWalkSpeed = 0;

    [Space(5)]
    [Header("Jump")]
    public float jumpForce = 1;

    //private fields
    private Rigidbody2D rb;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //walk
        myWalkSpeed = Mathf.Lerp(myWalkSpeed, walkSpeed * Input.GetAxisRaw("Horizontal"), walkAcc);
        rb.velocity = new Vector2(myWalkSpeed, rb.velocity.y);




        //jump
        if (Input.GetButtonDown("Jump"))
            rb.AddForce(new Vector2(0, jumpForce));
    }
}
