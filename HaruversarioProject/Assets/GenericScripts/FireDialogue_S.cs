using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireDialogue_S : MonoBehaviour
{
    public GameObject canvasElement;
    private TalkableBehaviour_S conversationScript;
    private void Start()
    {
        conversationScript = GetComponent<TalkableBehaviour_S>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Interact") && !conversationScript.dialogueActive)
        {
            conversationScript.dialogueActive = true;
            createConversationCanvas();
        }
    }

    void createConversationCanvas()
    {

    }
}
