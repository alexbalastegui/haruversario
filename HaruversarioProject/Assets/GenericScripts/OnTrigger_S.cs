using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTrigger_S : MonoBehaviour
{
    public UnityEvent OnEnterEv=new UnityEvent();
    public UnityEvent OnExitEv=new UnityEvent();
    public UnityEvent OnStayEv = new UnityEvent();

    // Start is called before the first frame update
    void Start()
    {
        if (OnEnterEv == null)
            OnEnterEv = new UnityEvent();
        if (OnExitEv == null)
            OnExitEv = new UnityEvent();
        if (OnStayEv == null)
            OnStayEv = new UnityEvent();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        OnEnterEv.Invoke();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        OnExitEv.Invoke();
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        OnStayEv.Invoke();
    }

}
