using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyTypes
{
    [System.Serializable]
    public class Communication
    {
        public string transmitter;
        public string message;
    }
}